import './character.class.dart';

class Bot extends Character {
  Bot({String nickname = "Bot", int strenght = 1, int lifePoints = 100})
      : super(nickname: nickname, strenght: strenght, lifePoints: lifePoints);
}
