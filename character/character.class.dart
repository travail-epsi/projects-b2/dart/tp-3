import '../NE_PAS_TOUCHER/outputs.dart';

class Character {
  String _nickname;
  int _strenght;
  int _lifePoints;

  String get nickname => _nickname;

  set nickname(String value) {
    if (value.length >= 3) {
      _nickname = value;
    }
  }

  bool get isAlive => _lifePoints > 0;

  set isAlive(bool value) {
    switch (value) {
      case true:
        _lifePoints = 100;
        break;
      case false:
        _lifePoints = 0;
        break;
    }
  }

  int get strenght => _strenght;

  set strenght(int value) {
    if (value > 0) {
      _strenght = value;
    }
  }

  int get lifePoints => _lifePoints;

  set lifePoints(int value) {
    if (value > 0) {
      _lifePoints = value;
    }
  }

  Character(
      {String nickname = "Random", int strenght = 1, int lifePoints = 100}) {
    this.nickname = nickname;
    this.strenght = strenght;
    this.lifePoints = lifePoints;
  }

  void dealDamages(int dicesValue) {
    if (isAlive && lifePoints > dicesValue) {
      lifePoints -= dicesValue;
      display(getInfo());
    } else if (isAlive && lifePoints - dicesValue <= 0) {
      isAlive = false;
      display("${nickname} est mort ! ");
    }
  }

  String getInfo() {
    return "$nickname - $lifePoints - $strenght";
  }
}
