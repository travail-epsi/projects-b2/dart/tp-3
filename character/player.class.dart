import '../NE_PAS_TOUCHER/outputs.dart';
import './character.class.dart';
import '../item/inventory.class.dart';

class Player extends Character {
  Inventory _inventory = new Inventory();

  Player(String nickname, {int strenght = 1, int lifePoints = 100})
      : super(nickname: nickname, strenght: strenght, lifePoints: lifePoints);

  void reward(){
    this.isAlive = true;
    display("Tu as gagné une friandise !");
  }
}
