import 'NE_PAS_TOUCHER/user_input.dart';
import 'NE_PAS_TOUCHER/outputs.dart';
import 'character/bot.class.dart';
import 'dart:math';
import 'character/player.class.dart';

void main() {
  final p1 = Player(askNickName());
  display(
      "Bienvenue ${p1.nickname} ! Préparez vous à participer à une quête épique ! ");

  var botCounter = 0;
  do {
    Bot bot = Bot();

    //
    var currentChara = flipACoin();
    do {
      var dicesValue;
      if (currentChara == 1) {
        awaitPlayer();
        dicesValue = dices(name: p1.nickname);
        bot.dealDamages(dicesValue);
        currentChara = 2;
      } else {
        dicesValue = dices(name: bot.nickname);
        p1.dealDamages(dicesValue);
        currentChara = 1;
      }
    } while (bot.isAlive && p1.isAlive);
    //
    if (!bot.isAlive) {
      display("Vous avez vaincu votre adversaire ! Bien joué !");
      botCounter++;
      p1.reward();
    } else if (!p1.isAlive) {
      display("GAME OVER !");
      break;
    }
  } while (p1.isAlive);
  display("Nombre de Bots tués $botCounter");
}

int dices({String name = "", int min = 2, int max = 12}) {
  var r = new Random();
  var dicesValue = r.nextInt(max - min + 1) + min;
  display("$name, vous avez obtenu $dicesValue au dés.");
  return dicesValue;
}

String askNickName() {
  var pseudo = "";
  do {
    pseudo = readText("Quel est votre nom aventurier ?");
  } while (pseudo.length < 3);
  return pseudo;
}

void awaitPlayer() {
  var input;
  do {
    input = readText("Appuyez sur entrer pour continuer");
  } while (input != "");
}

int flipACoin() {
  var r = new Random();
  return r.nextInt(2);
}
